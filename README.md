# Testing with docker compose

Trying out how to run tests with using docker-compose to start up 
dependencies and the tested services also.

## Service overview

The service is a Flask-based application which used DynamoDB as a backend.

The local dev server and the tests expose different ports to the host to make
it possible to run them at the same time.

### Local dev server

Use the following command to run the local dev server:

```bash
./run_dev.sh
```

Ports exposed for the host:

 * 8001: local DynamoDB
 * 5002: Flask app

Ports exposed on docker-compose network:

 * local-db:8000
 * dev-server:5000

The source code is mounted to the Flask service to allow automatic code reload
on source code change.

### Test runner

We use **tox** to run the test sets. We have the following environments in tox:

 * **py36**: Unit tests.
 * **black**: Checks that the code formatting is conform with black.
 * **flake8**: Flake8 style enforcement.
 * **func_test**: Functional tests.

To install tox execute the following command:

```bash
pip install tox
```

#### Unit tests

Use the following command to run the unit tests:

```bash
tox -e py36
```

#### Functional tests

Use the following command to run the tests:

```bash
tox -e func_test
```

Ports exposed for the host:

 * 8000: local DynamoDB
 * 5000: Flask app in "test" mode
 * 5001: Flask app in "production" mode

Ports exposed on docker-compose network:

 * local-db:8000
 * test-server:5000
 * prod-server:5000

### Ports overview

| Service   | Dev  | Test      |
|-----------|------|-----------|
| DynamoDB  | 8001 | 8000      |
| Flask app | 5002 | 5000,5001 |

## Functional test flow

Running the tests contains the following steps:

  * Starting up the local DynamoDB service.
  * For each configuration:
    * Filling the database with data.
    * Starting up/restarting two test services (one in test and one in
      production mode)
    * Running the test cases against the services.
  * Shutting down the services.

### Collecting the test configurations
The **run_functional.sh** script collects all related tests for each expected
server configuration.

A server configuration is defined with the **conf** pytest marker:

```python
@pytest.mark.conf(Data={'105': {'val1': 'conf_2', 'val2': 105, 'val3': '<server-mode>-conf_2-105'}})
```

This configuration is added to the default server configuration in
the **init_db.py** script.

Note: The **<server-mode>** string is replaced with the current server mode
(test|prod) in the **init_db.py** script.

We use two pytest hook to make this functionality work

 * **pytest_collection_modifyitems**: To collect the actual server
   configurations OR to select which test to run based on the configuration.
 * **pytest_deselected**: To remove deselected items from the collected
   configurations.
 * **pytest_sessionfinish**: To create the test configuration files in
   test collect mode.
