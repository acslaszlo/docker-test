"""Helper module to check whether the given services are up and running."""

import argparse
from datetime import datetime, timedelta
import json
import time
from typing import Dict
from threading import Thread

import requests

TIMEOUT = timedelta(seconds=10)
failed = set()


def parse_parameters() -> Dict[str, str]:
    """Parse the command line arguments.

    :return: The parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--servers", help="The path for the servers configuration file")

    args = parser.parse_args()

    if args.servers is None:
        raise ValueError("Missing required parameter: servers")

    return {"servers": args.servers}


def check_service(started: datetime, url: str) -> None:
    """Check the given URL.

    A service is considered to be OK if it returns a 2xx response within the global TIMEOUT.

    :param started: When the checking has been started.
    :param url: The service check URL.
    """
    while True:
        if datetime.utcnow() - started > TIMEOUT:
            failed.add(url)
            break

        try:
            r = requests.get(url=url, timeout=1)

            if r.status_code // 100 == 2:
                print(f"{url} is OK")

                break
            else:
                print(f"{url} failed with {r.status_code}")
                time.sleep(1)
        except Exception as e:
            print(f"{url} failed with {e}")
            time.sleep(1)


params = parse_parameters()
with open(params["servers"], "r") as f:
    servers = json.load(f)
urls = [server["healthcheck"] for server in servers.values()]
started = datetime.utcnow()

print(f"Checking the following services: {urls}")
threads = [Thread(target=check_service, kwargs={"started": started, "url": url}) for url in urls]

for t in threads:
    t.start()

for t in threads:
    t.join()

if len(failed):
    raise TimeoutError(f"The following services failed to respond in time: {list(sorted(failed))}")

print("All services are OK")
