#!/bin/bash

set -e

COMPOSE_BASE_FILE='tests/functional/docker-compose-base.yml'
COMPOSE_SERVICE_FILE='tests/functional/docker-compose-service.yml'
CONF_DIR='tests/functional/_confs'
CONF_FILES="${CONF_DIR}/conf_*.json"
CONF_MESSAGES_FILE='/tmp/conf_gen_messages.txt'
FIRST_CONF=0
SERVERS='tests/functional/servers.json'

# Let's brute force the exposed ports. Should be used a cooler solution but it will do for now.
cat docker-compose-base.yml | sed 's|8001:8000|8000:8000|' > "${COMPOSE_BASE_FILE}"

function msg() {
    echo
    echo "======================================="
    echo "  $1"
    echo "======================================="
    echo
}

function finish() {
    msg "Stopping docker compose"
    docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" down
}

trap finish EXIT

# Collect the test configurations
msg "Collect the test configurations"
rm -f ${CONF_FILES}

set +e
pytest tests/functional \
    --collect-only \
    --servers "${SERVERS}" \
    "$@" > "${CONF_MESSAGES_FILE}"

if [[ $? -ne 0 ]]; then
    echo "Failed to collect the test configurations"
    echo

    cat "${CONF_MESSAGES_FILE}"

    exit 1
fi

echo "Done"

set -e

msg "Starting local db"
docker-compose -f "${COMPOSE_BASE_FILE}" up --build -d

RES=0

for conf_file in `ls ${CONF_FILES}`; do
    msg "Initializing the db with ${conf_file}"
    PYTHONPATH=. \
        python tests/functional/init_db.py \
        --db-port 8000 \
        --conf "${conf_file}"

    if [[ ${FIRST_CONF} -eq 0 ]]; then
        msg "Staring docker compose"
        docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" up --build -d
        docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" logs -f > logs.txt &

        FIRST_CONF=1
    else
        msg "Restarting docker compose"
        docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" restart test-server prod-server
    fi

    msg "Waiting for health check result"
    python check_services.py \
        --servers "${SERVERS}"

    msg "Running the tests"
    pytest tests/functional \
        --related-tests "${conf_file}" \
        --servers "${SERVERS}" \
        "$@"

    if [[ $? -ne 0 ]]; then
        RES=1
    fi
    set -e
done

exit ${RES}
