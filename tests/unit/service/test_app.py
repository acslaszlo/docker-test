"""Unit tests for the application."""


class TestHealth:
    """Test suit for the health endpoint."""

    def test_health(self, client) -> None:
        """Check the result of the health endpoint.

        :param client: The test client.
        """
        res = client.get("/health")

        assert res.status_code == 200
        assert res.json == {"status": "success"}
