"""Global functions and fixtures for the service related tests."""

from unittest.mock import MagicMock

import pytest

from service.app import app as service_app


@pytest.fixture
def app():
    """:return: The test Flask application for the client fixture."""
    service_app.db = MagicMock()

    return service_app
