"""Example functional tests."""

import pytest


class TestOne:
    """Test suit for checking basic endpoints."""

    def test_test(self, server) -> None:
        """Check the test endpoint on the service.

        :param server: The test server connection.
        """
        r = server.get(endpoint="test/12")

        assert r.status_code == 200
        assert r.json() == {"status": "success", "id": "12"}

    def test_db_found(self, server) -> None:
        """Check the db endpoint with an existing entry.

        :param server: The test server connection.
        """
        r = server.get(endpoint="db/53")

        assert r.status_code == 200
        assert r.json() == {"status": "success", "id": "53", "val1": "53", "val2": 53, "val3": f"{server.mode}-53"}

    def test_not_found(self, server) -> None:
        """Check the db endpoint with a non-existing entry.

        :param server: The test server connection.
        """
        r = server.get(endpoint="db/100")

        assert r.status_code == 404
        assert r.json() == {"status": "error", "message": "Item 100 not found"}


class TestConfigs:
    """Test suit for different server configurations."""

    @pytest.mark.conf(Data={"105": {"val1": "conf_1", "val2": 105, "val3": "<server-mode>-conf_1-105"}})
    def test_conf_1(self, server) -> None:
        """Check that the server is in the first configuration.

        :param server: The test server connection.
        """
        r = server.get(endpoint="db/105")

        assert r.status_code == 200
        assert r.json() == {
            "status": "success",
            "id": "105",
            "val1": "conf_1",
            "val2": 105,
            "val3": f"{server.mode}-conf_1-105",
        }

    @pytest.mark.conf(Data={"105": {"val1": "conf_2", "val2": 105, "val3": "<server-mode>-conf_2-105"}})
    def test_conf_2(self, server) -> None:
        """Check that the server is in the second configuration.

        :param server: The test server connection.
        """
        r = server.get(endpoint="db/105")

        assert r.status_code == 200
        assert r.json() == {
            "status": "success",
            "id": "105",
            "val1": "conf_2",
            "val2": 105,
            "val3": f"{server.mode}-conf_2-105",
        }
