"""Helper module to initialize the service configuration."""

import argparse
from datetime import datetime
import json
from typing import Any, Dict

from flywheel import Engine, Model

import service.models as models


parser = argparse.ArgumentParser()
parser.add_argument("--db-port", type=int, help="The port for local DynamoDB")
parser.add_argument("--conf", type=str, help="Server configuration")


def safe_replace(orig: Any, replace_with: str) -> Any:
    """Replace the template with the given string if the original value is a string.

    :param orig: The original value.
    :param replace_with: The strign to replace with.
    :return: The replace string or the original value if it was not a string.
    """
    if isinstance(orig, str):
        return orig.replace("<server-mode>", replace_with)

    return orig


def write_data(namepsace: str, val3_base: str, port: int, conf: Dict) -> None:
    """Write the given configuration into the database.

    :param namepsace: The flywheel namespace.
    :param val3_base: The value for the <server-mode> placeholder.
    :param port: The database port.
    :param conf: The configuration to store.
    """
    db = Engine(namespace=namepsace)
    db.connect("local", access_key="AK", secret_key="SK", host="localhost", port=port, is_secure=False)

    classes = []
    for x in dir(models):
        x = getattr(models, x)
        try:
            if x is not Model and issubclass(x, Model):
                classes.append(x)
        except TypeError:
            pass

    for cls in classes:
        db.register(cls)
    db.create_schema()

    items = []
    for type_name, values in conf.items():
        cls = getattr(models, type_name)
        items += [
            cls(
                **{
                    safe_replace(orig=k, replace_with=val3_base): safe_replace(orig=v, replace_with=val3_base)
                    for k, v in value.items()
                }
            )
            for value in values.values()
        ]

    started = datetime.utcnow()
    db.save(items=items)

    elapsed = datetime.utcnow() - started
    print(f"{namepsace} elapsed time: {elapsed}")


args = parser.parse_args()

if args.db_port is None:
    raise ValueError("--db-port has not been set")

with open(args.conf, "r") as f:
    cfg = json.load(f)

write_data(namepsace="Test", val3_base="test", port=args.db_port, conf=cfg["config"])
write_data(namepsace="Prod", val3_base="prod", port=args.db_port, conf=cfg["config"])
