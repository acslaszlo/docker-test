"""Shared functionalities for the functional tests."""

from collections import defaultdict
from copy import deepcopy
from itertools import count
import json
from typing import Dict, List

import requests

_DEFAULT_CONFIG = {"Data": {"53": {"val1": "53", "val2": 53, "val3": "<server-mode>-53"}}}


def merge(source, destination):
    """Stolen from https://stackoverflow.com/a/20666342 .

    Run me with nosetests --with-doctest file.py

    >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
    >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
    >>> merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
    True
    """
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value

    return destination


def pytest_addoption(parser):
    """Add custom arguments to pytest."""
    parser.addoption("--related-tests", help="The related test data file")
    parser.addoption("--servers", action="store", help="The server configurations file path.")


_configs = defaultdict(list)
_related_tests = None


def _get_related_tests(fname: str) -> List[str]:
    """Read the test names from the given file.

    :param fname: The configuration data name.
    :return: The list of the tests names for this configuration.
    """
    global _related_tests

    if _related_tests is None:
        with open(fname, "r") as f:
            _related_tests = json.load(f)["tests"]

    return _related_tests


def _get_name_for_item(item) -> str:
    """Create the name for a test item.

    :param item: The test item.
    :return: The name of the test item.
    """
    return f"{item.fspath} - {item.cls} - {item.name}"


class TestServer:
    """Basic test server to access the services."""

    def __init__(self, name: str, config: Dict):
        """Create the test server.

        :param name: The name of the server.
        :param config: The server configuration.
        """
        self._name = name
        self._conf = config

    @property
    def name(self) -> str:
        """:return: The name of the server."""
        return self._name

    @property
    def base_url(self) -> str:
        """:return: The base url for the service."""
        return self._conf["base_url"]

    @property
    def mode(self) -> str:
        """:return: The associated service type (test/prod/...)."""
        return self._conf["mode"]

    def url_for(self, endpoint: str) -> str:
        """Compose the complete url for the given endpoint.

        :param endpoint: The service endpoint.
        :return: The complete url.
        """
        return f"{self.base_url}/{endpoint}"  # TODO: remove duplicated //

    def get(self, endpoint: str, *args, **kwargs):  # TODO: return type
        """Execute a get call against the tested service.

        :param endpoint: The service endpoint.
        :param args: The non-keyword arguments.
        :param kwargs: The keyword arguments.
        :return: The service response.
        """
        return self._call(method="get", endpoint=endpoint, *args, **kwargs)

    def _call(self, method: str, endpoint: str, *args, **kwargs):  # TODO: return type
        """Execute a call against the tested service.

        :param method: The called http method (get, post, ...).
        :param endpoint: The service endpoint.
        :param args: The non-keyword arguments.
        :param kwargs: The keyword arguments.
        :return: The service response.
        """
        return getattr(requests, method)(url=self.url_for(endpoint=endpoint), *args, **kwargs)


def pytest_generate_tests(metafunc) -> None:
    """Add the server object to those test cases which want to use it.

    :param metafunc: The pytest metafunction.
    """
    with open(metafunc.config.option.servers, "r") as f:
        configs = json.load(f)

    servers = [TestServer(name=name, config=conf) for name, conf in configs.items()]

    if "server" in metafunc.fixturenames:
        metafunc.parametrize("server", servers, ids=lambda x: x.name)


def pytest_collection_modifyitems(config, items) -> None:
    """Use the pythest hook to collect the test configurations or remove the non-related tests.

    Supports two running modes:

    1) Collect all test names for each configurations if runs in collect-only mode.
    2) Remove those tests that are not related to this configuration if runs not in collect-only mode.

    :param config: The pytest configuration.
    :param items: The test items.
    """
    global _configs

    collecting = config.getoption("--collect-only")

    if not collecting:
        related_tests = _get_related_tests(fname=config.getoption("--related-tests"))
        kept_tests = []

    for item in items:
        name = _get_name_for_item(item=item)

        if collecting:
            conf_marker = item.get_closest_marker("conf")
            cfg = deepcopy(_DEFAULT_CONFIG)

            if conf_marker:
                cfg = merge(conf_marker.kwargs, cfg)

            for x in cfg["Data"]:
                cfg["Data"][x]["id"] = x

            _configs[json.dumps(cfg, sort_keys=True)].append(name)
        else:
            if name in related_tests:
                kept_tests.append(item)

    if not collecting:
        items.clear()

        for item in kept_tests:
            items.append(item)


def pytest_deselected(items) -> None:
    """Remove the deselected items from the configurations.

    :param items: The deselected items.
    """
    if _configs:
        for item in items:
            name = _get_name_for_item(item=item)

            for values in _configs.values():
                try:
                    values.remove(name)
                except ValueError:
                    pass  # Ignore the "item not in list" error


def pytest_sessionfinish(session, exitstatus):
    """Create the configuration files if the configuration has been collected.

    It doesn't save a configuration if it has no tests.
    """
    if _configs:
        index = count(start=0)

        for cfg, tests in _configs.items():
            if tests:
                with open(f"tests/functional/_confs/conf_{next(index)}.json", "w") as f:
                    json.dump({"config": json.loads(cfg), "tests": tests}, f, indent=2)
