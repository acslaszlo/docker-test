#!/bin/bash

black \
    -l 120 \
    --exclude ".tox/|tox-venv/|venv/" \
    "$@"
