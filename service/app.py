"""Test service for test demonstration."""

import os

from flask import Flask, current_app, jsonify
from flywheel import Engine
from flywheel.query import EntityNotFoundException

from service.models import Data

app = Flask(__name__)
app._db = None


def _get_db() -> Engine:
    """:return: The database connection."""
    if app._db is None:
        app._db = Engine(namespace=os.environ["DB_NAMESPACE"])
        app._db.connect(
            "local",
            access_key="AK",
            secret_key="SK",
            host="local-db",
            port=8000,  # The application communicates on the docker-compose network with local DynamoDB
            is_secure=False,
        )

    return app._db


app.db = _get_db


@app.route("/health", methods=["GET"])
def health():
    """:return: The health-check result."""
    return jsonify({"status": "success"})


@app.route("/test/<id_>", methods=["GET"])
def test(id_: int):
    """An echo endpoint.

    :param id_: A test parameter to echo back.
    :return: The result with the given value.
    """
    return jsonify({"status": "success", "id": id_})


@app.route("/db/<id_>", methods=["GET"])
def db_endpoint(id_: int):
    """Read the requested entry from the database.

    :param id_: The entry id.
    :return: The entry value if there is such an entry. A 404 with an error message otherwise.
    """
    try:
        res = current_app.db()(Data).filter(id=id_).one()

        return jsonify({"status": "success", "id": res.id, "val1": res.val1, "val2": res.val2, "val3": res.val3})
    except EntityNotFoundException:
        return jsonify({"status": "error", "message": f"Item {id_} not found"}), 404
