#!/bin/bash

set -e

COMPOSE_BASE_FILE='docker-compose-base.yml'
COMPOSE_SERVICE_FILE='docker-compose-dev.yml'
VENV_DIR='dev-venv'

function msg() {
    echo
    echo "======================================="
    echo "  $1"
    echo "======================================="
    echo
}

function finish() {
    msg "Stopping docker compose"
    docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" down
}

trap finish EXIT

if [[ ! -e "${VENV_DIR}" ]]; then
    virtualenv -p python3.6 "${VENV_DIR}"

    . "${VENV_DIR}/bin/activate"

    pip install -r requirements.txt
    pip install docker-compose
else
    . "${VENV_DIR}/bin/activate"
fi

msg "Starting local db"
docker-compose -f "${COMPOSE_BASE_FILE}" up --build -d

msg "Initializing the db"
PYTHONPATH=. \
    python tests/functional/init_db.py \
    --db-port 8001 \
    --conf local_dev_config.json

msg "Staring docker compose"
docker-compose -f "${COMPOSE_BASE_FILE}" -f "${COMPOSE_SERVICE_FILE}" up --build
